/*

    LOGO DESTROYER

*/

let timesLogoClicked = 0

const logoWrapper = document.querySelector(".logo")
const logo = logoWrapper.querySelector("svg")
const svgElements = logoWrapper.querySelector("#c").childNodes
svgElements.forEach(ele => {
    ele.dataset.original = (ele.getAttribute("d") || ele.getAttribute("points"))
})

const destroyLogo = () => {
    svgElements.forEach(ele => {
        
        
        if(Math.random() < 0.8) {return}
        
        let points = (ele.getAttribute("d") || ele.getAttribute("points")).split(/[, ]/)
        for (let i = 0; i < 3; i++) {
            points[
                Math.floor(Math.random() * points.length)
            ] += Math.random() * 20 - 20
        }

        if(ele.getAttribute("d")){
            ele.setAttribute("d", points)
        }
        
        if(ele.getAttribute("points")){
            ele.setAttribute("points", points)
            
        }
    })

    

    timesLogoClicked ++
    if(timesLogoClicked > 10) {
        timesLogoClicked = 0

        svgElements.forEach(ele => {
            if(ele.getAttribute("d")){
                ele.setAttribute("d", ele.dataset.original)
            }
            
            if(ele.getAttribute("points")){
                ele.setAttribute("points", ele.dataset.original)
            }    
        })
    }
}

logoWrapper.addEventListener("click", destroyLogo)
logoWrapper.addEventListener("touchstart", destroyLogo)  

/*

    ROUTING CODE

*/

//we are basically recoding the default behaviour of hash urls, but for static urls for SEO reasons
if (history.scrollRestoration) {
    history.scrollRestoration = "manual";
}

const navigateTo = (path, pushState = true) => {
    //empty strings work differnetly, push state needs a leading "/"" and scroll to wouldn't word (becayse there's no ID)
    if(path === "") {
        window.scrollTo(0, 0)

        if(pushState) {
            history.pushState(null, null, "/")
        }
        
        return
    }

    if(pushState){
        history.pushState(null, null, path)
    }

    document.getElementById(path).scrollIntoView({
        block: "start",
        behavior: "smooth"
    })
}

document.addEventListener('click', (event) =>  {
    const target = event.target
    if (target.tagName === 'A' && target.getAttribute('href').startsWith('#')) {
        event.preventDefault();
        navigateTo(target.getAttribute('href').slice(1))
    }
})

window.addEventListener("popstate", (event) => {
    //get the last part of target which is the "hash" part of url
    const url = event.target.location.href
    const lastPart = url.substring(url.lastIndexOf('/') + 1)

    navigateTo(lastPart, pushState = false)
})

document.addEventListener("DOMContentLoaded", (event) => {
    const url = window.location.href
    const path = url.substring(url.lastIndexOf('/') + 1)

    if(path === "") { return }

    document.getElementById(path).scrollIntoView({
        block: "start",
        behavior: "instant"
    })
})

/*

    GALLERY CLICKING CODE

*/

//tbh this code is bit dodgy xD
const allGalleryItems = 
    [...document.querySelectorAll(".item")]
    .concat([...document.querySelectorAll(".cd")])

allGalleryItems.forEach(item => {
    item.addEventListener("click", () => {
        // Calculate the scroll position to center the clicked item
        const scrollPosition = item.offsetLeft - (window.innerWidth - item.offsetWidth) / 2
  
        // Scroll the gallery to the calculated position
        // not exactly sexy code but it works: all items will have a wrapper and gallery
        item.parentElement.parentElement.scrollTo({
            left: scrollPosition,
            behavior: "smooth"
        })
    })
})

/* 

    SLIDING NAVBAR CODE

*/

//Magic number
const scrollThreshold = 200;

const SlideInNav = () => {
    const sidebar = document.querySelector("nav")
    
    window.addEventListener("scroll", () => {
        if (window.scrollY >= scrollThreshold) {
            //Sidebar
            sidebar.style.left = "10px"

            return;
        } 

        //Magic number
        sidebar.style.left = "-200px"
    })
}

//BACKGROUND TRANSITION GRADIENT CODE
const CreateGardients = (sections) => {
    
    for (let i = 0; i < sections.length; i++) {
        const s = sections[i]
        
        if (s.id == "index") {continue}
        if ( sections[i-1].id == "graphic") {continue} //TODO: clean this bodge
        if ( sections[i-1].id == "cd") {continue} //TODO: clean this bodge

        const transition = document.createElement("div")

        const sectionStyle = window.getComputedStyle(s)
        const previousStyle = window.getComputedStyle(sections[i-1])

        transition.style.backgroundImage = `linear-gradient(${previousStyle.backgroundColor}, ${sectionStyle.backgroundColor})`
        transition.style.width = "100%"
        transition.style.height = "20vh"

    /* -------------------------------------------------
        ~  ALTERNATIVE WAVY DESIGN ~
    ---------------------------------------------------- */
        //transition.style.backgroundColor = sectionStyle.backgroundColor
        // transition.innerHTML = `<svg mlns='http://www.w3.org/2000/svg' viewBox="0 0 64 64" style="height: 400px; width: 100%;" preserveAspectRatio="none">
        //     <path d='M0 10 C30 28 38 0 64 10 L64 0 L0 0 Z'  fill="${previousStyle.backgroundColor}"/></svg>`

        //this doesn't do anything, just to describe what it does in case anyones snooping
        transition.classList.add("gradient")

        s.insertAdjacentElement("beforebegin",transition)
    }
}

const sections = document.querySelectorAll("section")
CreateGardients(sections)
SlideInNav()

///REVEAL CODE, for contact links
const reveal = (element, text) => {
    element.innerHTML = text
}


/*

    SIMON CODE
    - it is so long because of touch and mouse
*/
const simon = document.querySelector("#simon")
simon.addEventListener('mousedown', (e) => {
    simon.src = window.woof 
})
simon.addEventListener('mouseup', (e) => {
    simon.src = window.dog 
})

let draggables = document.querySelectorAll(".draggable")

isMouseDown = false;

let offsetX = 0;
let offsetY = 0;
let mouseX = 0;
let mouseY = 0;

draggables.forEach(draggable => {
    // Mouse down event
    draggable.addEventListener('mousedown', (e) => {
        isMouseDown = true;
        draggable.classList.add("selected")
        offsetX = draggable.offsetLeft - e.clientX;
        offsetY = draggable.offsetTop - e.clientY;
    });
});

// Mouse move event
document.addEventListener('mousemove', (e) => {
    if(!isMouseDown) return;
    
    selected = document.querySelector(".selected")
    e.preventDefault(); 
    mouseX = e.clientX + offsetX;
    mouseY = e.clientY + offsetY;
    selected.style.left = mouseX + 'px';
    selected.style.top = mouseY + 'px';
});

// Mouse up event
document.addEventListener('mouseup', (e) => {
    isMouseDown = false;
    draggables.forEach(draggable => {draggable.classList.remove("selected")})
});

const isTouchnDevice = () => {
    return 'ontouchstart' in window || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;
}

if(isTouchnDevice()) {
    let isTouchDown = false;

    draggables.forEach(draggable => {
        // Touch start event
        draggable.addEventListener('touchstart', (e) => {
            isTouchDown = true;
            draggable.classList.add("selected");
            let touch = e.touches[0];
            offsetX = draggable.offsetLeft - touch.clientX;
            offsetY = draggable.offsetTop - touch.clientY;
        });
    });

    // Touch move event
    document.addEventListener('touchmove', (e) => {
        if (!isTouchDown) return;
        
        e.preventDefault();
        let touch = e.touches[0];
        let selected = document.querySelector(".selected");
        mouseX = touch.clientX + offsetX;
        mouseY = touch.clientY + offsetY;
        selected.style.left = mouseX + 'px';
        selected.style.top = mouseY + 'px';
    }, {passive: false});

    // Touch end event
    document.addEventListener('touchend', (e) => {
        isTouchDown = false;
        draggables.forEach(draggable => { draggable.classList.remove("selected"); });
    });
}