from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, render_template_string
)
from random import randrange

bp = Blueprint('index', __name__, url_prefix="/")

#html name, id name, display name
#is there a better way of doing this? probably :)
#maybe looking through a folder?
sections = [
    ('index', '', 'home'),
    ('profile', "adam", "me"),
    ('graphic', 'graphic', "graphic design"),
    ('cd', 'cd', ''),
    ('graphic2', 'graphic2', ''),
    ('youtube', 'youtube', "youtube"),
    ('sound', 'sound', 'sound design'),
    ('music', 'music', 'music'),
    ('footer', 'footer', '')]

def combine_templates():
    combined_content = ""
    nav = "<nav><ul>"

    for section, idname, display in sections:
        combined_content += f"\n{{% include '{section}.html' %}}"
        
        if display == "": #the display name is empty, meaning dont show it in the nav
            continue 

        nav += f"""
            <li class="{idname} nav-item" style="transform:rotate({randrange(-15,15)}deg)">
                <a href='#{idname}'>{display}</a>
            </li>
            """
    
    combined_content += nav + "</ul></nav>"
    combined_content += """<script src="{{url_for('static',filename='index.js')}}"></script>"""
    
    return render_template_string(combined_content)

@bp.route("/", methods=["GET"])
@bp.route("/<path:path>", methods=["GET"])
def index(path=None):
    return combine_templates()

