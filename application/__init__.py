from flask import (Flask, send_from_directory, request)

# flask --app application run --debug

def create_app():
    app = Flask(__name__)
    app.config.from_mapping(
        SECRET_KEY = "dev")
    
    from . import index
    app.register_blueprint(index.bp)

    return app

#thanks sam
app = create_app()

@app.route('/robots.txt')
@app.route('/sitemap.xml')
def static_from_root():
    return send_from_directory(app.static_folder, request.path[1:])